import {useEffect, useMemo, useState} from "react";
import "./App.css";

function Button(props) {
    console.count("render button");
    return <button {...props} style={{backgroundColor: "lightgray"}}/>;
}

function ListItem({value}) {
    console.count("render list item");
    return (
        <li>
            {value}
            <label style={{fontSize: "smaller"}}>
                <input type="checkbox"/>
                Add to cart
            </label>
        </li>
    );
}

function ProductList({ searchString, isSortingDesc }) {
    console.count("render product list");

    useEffect(() => {
        console.count("render fetch");
        fetch("https://reqres.in/api/products")
            .then((response) => response.json())
            .then((json) =>
                setProducts(
                    json.data
                        .filter((item) => item.name.includes(searchString))
                        .sort((a, z) =>
                            isSortingDesc
                                ? z.name.localeCompare(a.name)
                                : a.name.localeCompare(z.name)
                        )
                )
            );
    }, [searchString, isSortingDesc]);

    const [products, setProducts] = useState([]);

    const productList = useMemo(() => {
        return products.map((product) => {
            return <ListItem key={product.name} value={product.name}/>;
        })
    }, [products])

    if (products.length === 0) return null;

    return (
        <ul>
            {productList}
        </ul>
    );
}

function App() {
    const [searchString, setSearchString] = useState("");
    const [isSortingDesc, setSortingDesc] = useState(false);

    console.count("render app");

    const handleButtonClick = () => {
        setSortingDesc((value) => !value)
    }

    const handleSearchChange = (e) => {
        setSearchString(e.target.value)
    }

    return (
        <div className="App">
            <input
                type="search"
                value={searchString}
                onChange={handleSearchChange}
            />
            <Button
                onClick={handleButtonClick}
            >
                Change sort direction
            </Button>
            <ProductList
                searchString={searchString}
                isSortingDesc={isSortingDesc}
            />
        </div>
    );
}

export default App;
